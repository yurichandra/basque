# frozen_string_literal: true

categories = [
  { id: 1, name: 'Sports' },
  { id: 2, name: 'Arsenal' },
  { id: 3, name: 'Microservice' }
]

# Seeder of category
categories.each do |item|
  if Category.find_by(id: item[:id]).nil?
    Category.create(id: item[:id], name: item[:name])
  end
end
