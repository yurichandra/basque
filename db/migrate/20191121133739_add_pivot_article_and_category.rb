# frozen_string_literal: true

class AddPivotArticleAndCategory < ActiveRecord::Migration[6.0]
  def change
    create_table :articles_categories do |t|
      t.belongs_to :article
      t.belongs_to :category
    end
  end
end
