# frozen_string_literal: true

Rails.application.routes.draw do
  apipie
  get 'index/index'

  get 'categories', to: 'category#get'
  get 'categories/:id', to: 'category#find'
  post 'categories', to: 'category#create'
  patch 'categories/:id', to: 'category#update'
  delete 'categories/:id', to: 'category#delete'

  get 'articles', to: 'article#get'
  post 'articles', to: 'article#create'

  root 'index#index'
end
