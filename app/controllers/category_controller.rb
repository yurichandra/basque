# frozen_string_literal: true

require 'json'

class CategoryController < ApplicationController
  include CategoryService

  def get
    @categories = CategoryService.get

    render json: @categories
  end

  def find
    @category = CategoryService.find(id_category_param)

    render json: @category
  end

  def create
    @category = CategoryService.create(category_param)

    render json: @category
  end

  def update
    @category = CategoryService.update(id_category_param, category_param)

    render json: @category
  end

  def delete
    @category = CategoryService.delete(id_category_param)

    render json: JSON.generate({})
  end

  private

  def id_category_param
    params.require(:id)
  end

  def category_param
    params.require(:category).permit(:name)
  end
end
