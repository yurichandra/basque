# frozen_string_literal: true

require 'json'

class ArticleController < ApplicationController
  include ArticleService

  api :GET, "/articles", "Return all articles available."
  formats ['json']
  error 500, "Internal server error."
  def get
    @articles = ArticleService.get

    render json: @articles
  end

  api :POST, "/articles", "Store a new article."
  formats ['json']
  error 400, "Bad Request."
  error 422, "Unprocessable entity."
  error 500, "Internal server error."
  param :article, Hash, desc: "Article Details" do
    param :title, String, desc: "Title of an article", required: true
    param :body, String, desc: "Body of an article", required: true
    param :category_ids, Array, of: Integer, desc: "ID's of category", required: true
  end
  def create
    @article = ArticleService.create(
      article_params[:title],
      article_params[:body],
      article_params[:category_ids]
    )
    
    render json: @article
  end

  private
    def article_params
      params.require(:article).permit(:title, :body, :category_ids => [])
    end
end
