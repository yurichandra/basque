# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ActionController::Serialization

  rescue_from Apipie::ParamInvalid do |exception|
    render status: 422, json: {
      status: 422,
      error: exception.to_s
    }
  end

  rescue_from Apipie::ParamMissing do |exception|
    puts exception
    render status: 400, json: {
      status: 400,
      error: exception.to_s
    }
  end
end
