# frozen_string_literal: true

require 'json'

class IndexController < ApplicationController
  def index
    response = {
      name: 'basque-api',
      description: 'Learning project written in RoR'
    }

    render json: JSON.generate(response)
  end
end
