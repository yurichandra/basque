# frozen_string_literal: true

class Article < ApplicationRecord
  has_and_belongs_to_many :categories
  validates :title, :body, :categories, presence: true
end
