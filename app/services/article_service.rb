# frozen_string_literal: true

module ArticleService
  class << self
    def get
      @articles = Article.all
    end

    def create (title, body, category_ids)
      @article = Article.new
      @article.categories = CategoryService.find(category_ids)
      @article.title = title
      @article.body = body
      @article.save
      @article
    end
  end
end
