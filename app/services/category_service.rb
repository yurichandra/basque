# frozen_string_literal: true

# Module of CategoryService

module CategoryService
  class << self
    def get
      @category = Category.all
    end

    def find(id)
      @category = Category.find(id)
    end

    def create(name)
      @category = Category.new(name)
      @category.save
      @category
    end

    def update(id, name)
      @category = find(id)
      @category.update(name)
      @category
    end

    def delete(id)
      @category = find(id)
      @category.destroy
    end
  end
end
